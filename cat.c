#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>

#define MAX_BUFFER_SIZE (4 * 4096)

// Macro to check if two strings are equal
#define STREQ(a, b) (strcmp((a), (b)) == 0)

// Function to print an error message and exit
void error_exit(const char *message) {
    perror(message);
    exit(EXIT_FAILURE);
}

// Function to handle an error condition and exit if necessary
void handle_error(int condition, const char *message) {
    if (condition) {
        error_exit(message);
    }
}

// Function to read from input and write to output with optional features
void cat_file(int input_desc, int output_desc, int show_numbers, int show_nonprinting, int squeeze_blank) {
    char *inbuf = malloc(MAX_BUFFER_SIZE);    // Allocate input buffer
    char *outbuf = malloc(MAX_BUFFER_SIZE);   // Allocate output buffer

    handle_error(inbuf == NULL || outbuf == NULL, "Memory allocation error");

    ssize_t n_read, n_written;
    off_t total_written = 0;
    int line_number = 0;
    int total_lines = 0; // Counter for total lines

    // Loop to read from input and write to output
    while ((n_read = read(input_desc, inbuf, MAX_BUFFER_SIZE)) > 0) {
        // Copy input to output buffer
        strcpy(outbuf, inbuf);
        // Write to output
        n_written = write(output_desc, outbuf, strlen(outbuf));

        handle_error(n_written == -1, "Write error");

        total_written += n_read;
        
        // Count lines
        for (size_t i = 0; i < n_read; ++i) {
            if (inbuf[i] == '\n') {
                total_lines++;
            }
        }
    }

    handle_error(n_read == -1, "Read error");

    // Print total lines
    if (show_numbers) {
        printf("\nTotal lines: %d\n", total_lines);
    }

    free(inbuf);   // Free input buffer
    free(outbuf);  // Free output buffer
}


int main(int argc, char **argv) {
    char *infile = "-";
    int input_desc, output_desc;

    int show_numbers = 0;
    int show_nonprinting = 0;
    int squeeze_blank = 0;

    int opt;
    // Command-line argument parsing loop
    // Command-line argument parsing loop
    while ((opt = getopt(argc, argv, "neAsvTb")) != -1) {
    // Switch statement to handle different command-line options
        switch (opt) {
            case 'n':
            // Enable number of lines
                show_numbers = 1;
                break;
            case 'e':
            // Enable non-printing characters display
                show_nonprinting = 1;
                break;
            case 'A':
            // Enable both number of lines and non-printing characters display
                show_numbers = 1;
                show_nonprinting = 1;
                break;
            case 's':
            // Enable squeezing of blank lines
                squeeze_blank = 1;
                break;
            case 'v':
            // Enable non-printing characters display
                show_nonprinting = 1;
                break;
            case 'T':
            // Enable non-printing characters display
                show_nonprinting = 1;
                break;
            default:
            // Invalid option, print usage information and exit
                fprintf(stderr, "Usage: %s [-neAsvTb] [file ...]\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }


    if (optind < argc) {
        infile = argv[optind];
    }

    if (STREQ(infile, "-")) {
        input_desc = STDIN_FILENO;  // If input is '-', use standard input
    } else {
        input_desc = open(infile, O_RDONLY);  // Otherwise, open the specified file
        handle_error(input_desc == -1, "Failed to open input file");
    }

    output_desc = STDOUT_FILENO;  // Use standard output for writing

    struct stat stat_buf;
    handle_error(fstat(output_desc, &stat_buf) == -1, "Failed to get file status");

    // Check if the output is a regular file and is not the same as the input
    if (S_ISREG(stat_buf.st_mode)) {
        if (input_desc == output_desc && lseek(input_desc, 0, SEEK_CUR) < stat_buf.st_size) {
            error_exit("Input file is the same as output file");
        }
    }

    // Execute the cat_file function to read from input and write to output
    cat_file(input_desc, output_desc, show_numbers, show_nonprinting, squeeze_blank);

    if (input_desc != STDIN_FILENO) {
        handle_error(close(input_desc) == -1, "Failed to close input file");
    }

    return EXIT_SUCCESS;
}
