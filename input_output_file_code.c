#include <stdio.h>

int main() {
    FILE *file;
    char file_name[] = "Textfile.txt";
    char text[5000];

    printf("Enter text: ");
    scanf("%[^\n]", text);


    file = fopen(file_name, "w");
    fprintf(file, "%s", text);
    fclose(file);

    file = fopen(file_name, "r");

    char ch;
    printf("File text:\n");
    while ((ch = fgetc(file)) != EOF) {
        putchar(ch);
    }

    fclose(file);

    return 0;
}
