#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    srand((int)time(NULL));
    
    char replay;
    int limit = 7;
    int  score = 0, Input, guess, number, amount_of_attempts = 0;
    
    printf("Number Guessing Game!\n");


    do {
        number = rand() % 5000 + 1;
        amount_of_attempts = 0;
        limit = 3;

        printf("Guess the number between 1 and 5000 in %d attempts.\n", limit);
        // printf("Number %d", number);
            
         do {
            printf("Enter your guess number: ");

            Input = scanf("%d", &guess);
            
            
            if (Input != 1) {

                while (getchar() != '\n');
                printf("Invalid input. Please enter a valid number.\n");
                continue;
            }

            else if(guess >= 5000 || guess <= 1){
                printf("Number must be between 1 to 5000\n");
                continue;
            }
            limit--;

            amount_of_attempts++;
            if (guess == number) {
                printf("Yay! You have guessed the number in %d attempts.\n", amount_of_attempts);

                score += 1;
                break;
            } 
            
            else if (guess < number && limit != 0) {
                printf("Try higher.\n");
            }
            else if (guess < number && limit == 0) {
                printf("You failed. Game over\n");
            } 
            else if (guess > number && limit != 0) {
                printf("Try lower.\n");
            }
            else if (guess > number && limit == 0) {
                printf("You failed. Game over\n");
            }
        } while (1 && limit != 0);


        printf("Score: %d\n", score);
        printf("Do you want to play again? Enter y to continue, or anything else stop: ");
        scanf(" %c", &replay);

    } while (replay == 'y' || replay == 'Y');


    printf("\nYour Final score: %d\n", score);

    return 0;
}
